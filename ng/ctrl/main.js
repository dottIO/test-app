// main setting
var ngApp = angular.module('ngApp', []);

ngApp.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.which === 13) {
                scope.$apply(function(){
                        scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});

ngApp.controller('mainController', function($rootScope, $scope, $location, $interval, $anchorScroll) {
    $scope.calcDisplay = 0;
    var _calcTotal = 0;
    var _userInput = "";
    var _inputedOperation = "+";
    var _isNumber = false;
    
    $scope.inputValue = function(num){
        _isNumber = true;                      
        _userInput += num;
        $scope.calcDisplay = _userInput;
    };

    $scope.calcrate = function(operator){
        if (_isNumber == true) {
            _isNumber = false;
            var evalStr = _calcTotal + _inputedOperation + _userInput;
            _calcTotal = eval(evalStr);
            _userInput = "";
            $scope.calcDisplay = _calcTotal;
        }
        if(operator == "="){
            if(_inputedOperation == "="){
                _calcTotal = 0;
                _userInput = "";
            }
            _inputedOperation = "=";
        }else{
            _inputedOperation = operator;
        }
    };
    
    $scope.clear = function(){
        _calcTotal = 0;
        _inputedOperation = "+";
        _userInput = "";
        $scope.calcDisplay = _calcTotal;
    }
});
